﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UniRx;
using UniRx.Triggers;

namespace HerdsmanGame
{
    public class SheepController : MonoBehaviour
    {
        [Inject]
        public ISheep sheep;

        [Inject]
        public void Construct(ISheep sheep)
        {
            this.sheep = sheep;
        }
        // Use this for initialization
        void Start()
        {
            sheep = new Sheep();
            sheep.GameObject = this.gameObject;
        }

        // Update is called once per frame
        void Update()
        {

        }

        public class Factory : PlaceholderFactory<SheepController> { }
    }
}
