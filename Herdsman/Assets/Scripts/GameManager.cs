﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;
using Zenject;

namespace HerdsmanGame
{
    public interface IGameManager
    {
        ReactiveProperty<float> GameTime
        {
            get;
            set;
        }
        bool GameEnded
        {
            get;
            set;
        }
        ReactiveProperty<int> Score
        {
            get;
            set;
        }
    }

    public class GameManagerModel : IGameManager
    {
        private ReactiveProperty<float> gameTime;
        private bool gameEnded;
        private ReactiveProperty<int> score;

        public ReactiveProperty<float> GameTime
        {
            get
            {
                return gameTime;
            }
            set
            {
                gameTime = value;
            }
        }

        public ReactiveProperty<int> Score
        {
            get
            {
                return score;
            }
            set
            {
                score = value;
            }
        }

        public bool GameEnded
        {
            get
            {
                return gameEnded;
            }
            set
            {
                gameEnded = value;
            }
        }
    }

    public class GameManager : MonoBehaviour
    {
        [Inject]
        IGameManager gameManagerModel;
        [Inject]
        IUserInput userInput;
        [SerializeField]
        private float maxGameTime;
        public IReadOnlyReactiveProperty<bool> gameEnded { get; private set; }

        void Awake()
        {

        }

        // Use this for initialization
        void Start()
        {
            gameManagerModel.GameTime = new ReactiveProperty<float>(maxGameTime);
            gameManagerModel.Score = new ReactiveProperty<int>(0);

            var timeObservable = Observable.Interval(TimeSpan.FromSeconds(1))
                .TakeWhile(_ => gameManagerModel.GameTime.Value > 0)
                .Subscribe(xs => {gameManagerModel.GameTime.Value -= 1;
                });

            var gameEndedObservable = Observable.EveryUpdate()
                .Where(_ => gameManagerModel.GameTime.Value < 1)
                .Subscribe(_ => { gameManagerModel.GameEnded = true;
                userInput.InputBlocked = true;
                });
        }
    }
}
