﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserInput : IUserInput {

    private Vector2 pointClicked;
    private bool clicked;
    private bool inputBlocked;

    public bool InputBlocked
    {
        get
        {
            return this.inputBlocked;
        }
        set
        {
            this.inputBlocked = value;
        }
    }

    public Vector2 PointClicked
    {
        get
        {
            return this.pointClicked;
        }
        set
        {
            this.pointClicked = value;
        }
    }

    public bool Clicked
    {
        get
        {
            return clicked;
        }
        set
        {
            this.clicked = value;
        }
    }
}
