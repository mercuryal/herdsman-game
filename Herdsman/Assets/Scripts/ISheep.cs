﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HerdsmanGame
{
    public interface ISheep
    {
        GameObject GameObject { get; set; }
        float Speed { get; set; }
    }
}
