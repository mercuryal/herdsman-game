﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UniRx;
using UniRx.Triggers;
using System.Linq;

namespace HerdsmanGame
{
    public class Sheep: ISheep
    {
        private float speed;
        private GameObject gameObject;

        public Sheep()
        {

        }

        public GameObject GameObject
        {
            get 
            {
                return gameObject;
            }
            set 
            {
                gameObject = value;
            }
        }

        public float Speed
        {
            get
            {
                return speed;
            }
            set
            {
                speed = value;
            }
        }
    }

    public class CattleComponent : Sheep
    {

        public CattleComponent() : base() 
        {
            cattleSheeps = new List<ISheep>();
        }
        public List<ISheep> cattleSheeps;

        public void add(ISheep entity)
        {
            cattleSheeps.Add(entity);
        }

        public void remove(ISheep entity)
        {
            cattleSheeps.Remove(entity);
            entity.GameObject.SetActive(false);
        }

        public void moveTo(Vector2 position)
        {
            foreach (var item in cattleSheeps)
            {
                item.GameObject.transform.position = Vector2.MoveTowards(item.GameObject.transform.position, position, item.Speed);
            }
        } 

    }
    public class PlayerController : MonoBehaviour
    {
        [Inject]
        public IUserInput userInput;
        [Inject]
        IField field;
        [Inject]
        IGameManager gameManager;
        [SerializeField]
        int maxGather;
        public float speed;
        CattleComponent cattle;
        bool moving;
        bool unloading;

        void Awake()
        {
            cattle = new CattleComponent();
        }

        // Use this for initialization
        void Start()
        {
            //movement observables
            var inputObservable = Observable.EveryUpdate()
                .Where(_ => userInput.Clicked && !unloading)
                .Subscribe(_ => moving = true);

            var movementObservable = Observable.EveryUpdate()
                .Where(_ => moving)
                .Where(_ => (userInput.PointClicked - new Vector2(transform.position.x, transform.position.y)).sqrMagnitude > 0.01f)
                .Subscribe(_ => moveToPoint(userInput.PointClicked));

            var stopMovingObservable = Observable.EveryUpdate()
                .Where(_ => moving)
                .Where(_ => (userInput.PointClicked - new Vector2(transform.position.x, transform.position.y)).sqrMagnitude <= 0.01f)
                .Subscribe(_ => moving = false);

            //triggers
            var sheepsGatherObservable = gameObject.OnTriggerEnter2DAsObservable()
                .Where(x => x.gameObject.GetComponent<SheepController>() && cattle.cattleSheeps.Count < maxGather)
                .Select(xs => xs.gameObject.GetComponent<SheepController>().sheep);

            var animalPenObservable = gameObject.OnTriggerEnter2DAsObservable()
                .Where(x => x.gameObject.GetComponent<AnimalPen>());

            sheepsGatherObservable.Subscribe(x => { cattle.add(x); x.Speed = speed; field.AnimalsOnField -= 1; });

            animalPenObservable.Subscribe(x => StartCoroutine("shipAnimals"));
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        void moveToPoint(Vector2 point)
        {
            transform.position = Vector2.MoveTowards(transform.position, point, speed);
            
            //make a small distance from character and sheep
            Vector2 sheepPosition = new Vector2(point.x - 0.6f, point.y);

            cattle.moveTo(sheepPosition);
        }

        public IEnumerator shipAnimals()
        {
            unloading = true;
            while (cattle.cattleSheeps.Count > 0)
            {
                cattle.remove(cattle.cattleSheeps.First());
                gameManager.Score.Value += 1;
            }
            yield return new WaitForSeconds(0.5f);
            unloading = false;
        }
    }
}
