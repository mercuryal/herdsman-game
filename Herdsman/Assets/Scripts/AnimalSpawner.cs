﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System.Linq;
using UniRx;

namespace HerdsmanGame
{
    public interface IField
    {
        int AnimalsOnField { get; set; }
    }

    public class Field : IField
    {
        private string message = "Hello zenject, I want to work with crazy panda";
        private int animalsOnField;

        public int AnimalsOnField
        {
            get
            {
                return animalsOnField;
            }
            set
            {
                animalsOnField = value;
            }
        }
    }

    public class AnimalSpawner : MonoBehaviour
    {
        [Inject]
        IField field;
        [Inject]
        SheepController.Factory sheepControllerFactory;
        public Transform[] spawnPoints;
        public int maxAnimalsOnTheField;
        List<Transform> spawnPointsList;
        List<GameObject> animalPool;

        // Use this for initialization
        void Start()
        {
            spawnPointsList = spawnPoints.ToList();
            animalPool = new List<GameObject>();

            var poolCreation = Observable.EveryUpdate()
                .TakeWhile(_ => animalPool.Count < 20)
                .Subscribe(_ => createAnimal());

            var spawnObservable = Observable.EveryUpdate()
                .Where(_ => field.AnimalsOnField < maxAnimalsOnTheField)
                .Subscribe(_ => spawnAnimal());
        }

        // Update is called once per frame
        void Update()
        {

        }

        void createAnimal()
        {
            int randomSpotNumber = Random.Range(0, spawnPoints.Count());
            var newSheepController = sheepControllerFactory.Create();
            newSheepController.transform.parent = transform;
            animalPool.Add(newSheepController.gameObject);
            newSheepController.gameObject.SetActive(false);
        }

        void spawnAnimal()
        {
            int randomSpotNumber = Random.Range(0, spawnPoints.Count());
            GameObject selectedAnimal = animalPool.Where(x => x.activeSelf == false).First();
            selectedAnimal.transform.position = spawnPointsList[randomSpotNumber].position;
            selectedAnimal.SetActive(true);
            field.AnimalsOnField += 1;
        }
    }
}
