using UnityEngine;
using Zenject;
using HerdsmanGame;

public class GameInstaller : MonoInstaller
{
    public SheepController sheepControllerPrefab;
    public override void InstallBindings()
    {
        Container.Bind<IUserInput>().To<UserInput>().AsSingle();
        Container.Bind<HerdsmanGame.ISheep>().To<HerdsmanGame.Sheep>().AsSingle();
        Container.Bind<HerdsmanGame.IField>().To<HerdsmanGame.Field>().AsSingle();
        Container.Bind<HerdsmanGame.IGameManager>().To<HerdsmanGame.GameManagerModel>().AsSingle();
        Container.BindFactory<SheepController, SheepController.Factory>().FromComponentInNewPrefab(sheepControllerPrefab);
    }
}