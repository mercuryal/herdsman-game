﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUserInput
{
    Vector2 PointClicked
    {
        get;
        set;
    }
    bool Clicked
    {
        get;
        set;
    }
    bool InputBlocked
    {
        get;
        set;
    }
}
