﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using Zenject;
using HerdsmanGame;

public class ReactivePresenter : MonoBehaviour {
    [SerializeField]
    Text score;
    [SerializeField]
    Text time;
    [SerializeField]
    GameObject gameOverWindow;
    [Inject]
    IGameManager gameManager;

	// Use this for initialization
	void Start () {
        gameManager.GameTime.SubscribeToText(time);
        gameManager.Score.SubscribeToText(score);

        var gameOverObservable = Observable.EveryUpdate()
            .Where(_ => gameManager.GameEnded)
            .Subscribe(_ => gameOverWindow.SetActive(true));
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
