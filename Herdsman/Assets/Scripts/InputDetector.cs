﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UniRx;

public class InputDetector : MonoBehaviour {
    [Inject]
    public IUserInput userInput;
	// Use this for initialization
	void Start () {

        var clickObservable = Observable.EveryUpdate()
            .Where(_ => !userInput.InputBlocked)
            .Subscribe(_ => userInput.Clicked = Input.GetMouseButtonUp(0));

        var clickPointObservable = Observable.EveryUpdate()
            .Where(_ => userInput.Clicked)
            .Subscribe(x => userInput.PointClicked = getMousePosition());
	}
	
	// Update is called once per frame
	void Update () {
       
	}

    Vector2 getMousePosition()
    {
        Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        userInput.Clicked = true;
        return new Vector2(position.x, position.y);
    }
}
