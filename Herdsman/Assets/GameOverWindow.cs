﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using HerdsmanGame;

public class GameOverWindow : MonoBehaviour {

	// Use this for initialization
	void Start () {
		 
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void reloadScene()
    {
        gameObject.SetActive(false);
        SceneManager.LoadScene(0);
    }
}
